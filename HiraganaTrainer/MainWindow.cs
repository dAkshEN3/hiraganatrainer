﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace HiraganaTrainer
{
    public partial class MainWindow : Form
    {
        private static string StatsFile = "Stats.dat";
        private static string[] Ratings = new string[] { "E", "D", "C", "B", "A" };

        private Random Rand;
        private HiraganaCharacter[] HiraganaCharacters;
        private int CurrentChar;

        public MainWindow()
        {
            Rand = new Random();
            
            PopulateHiraganaCharacters();

            InitializeComponent();

            PopulateStats();
            LoadStats();

            TrainerButtonCheck.Enabled = false;
            TrainerButtonDontKnow.Enabled = false;
            TrainerRomajiInput.Enabled = false;
            TrainerButtonNext.Enabled = true;
        }

        private void WindowClosing(object sender, FormClosingEventArgs e)
        {
            SaveStats();
        }

        #region Population

        /// <summary>
        /// Populates the HiraganaCharacters array with characters.
        /// </summary>
        private void PopulateHiraganaCharacters()
        {
            HiraganaCharacters = new HiraganaCharacter[]
            {
                //Lone Vowels
                new HiraganaCharacter("あ", "a", 0, 0),
                new HiraganaCharacter("い", "i", 0, 0),
                new HiraganaCharacter("う", "u", 0, 0),
                new HiraganaCharacter("え", "e", 0, 0),
                new HiraganaCharacter("お", "o", 0, 0),

                //K* Monographs
                new HiraganaCharacter("か", "ka", 0, 0),
                new HiraganaCharacter("き", "ki", 0, 0),
                new HiraganaCharacter("く", "ku", 0, 0),
                new HiraganaCharacter("け", "ke", 0, 0),
                new HiraganaCharacter("こ", "ko", 0, 0),

                //Ky* Diagraphs
                new HiraganaCharacter("きゃ", "kya", 0, 0),
                new HiraganaCharacter("きゅ", "kyu", 0, 0),
                new HiraganaCharacter("きょ", "kyo", 0, 0),

                //S* Monographs
                new HiraganaCharacter("さ", "sa", 0, 0),
                new HiraganaCharacter("し", "shi", 0, 0),
                new HiraganaCharacter("す", "su", 0, 0),
                new HiraganaCharacter("せ", "se", 0, 0),
                new HiraganaCharacter("そ", "so", 0, 0),

                //Sh* Diagraphs
                new HiraganaCharacter("しゃ", "sha", 0, 0),
                new HiraganaCharacter("しゅ", "shu", 0, 0),
                new HiraganaCharacter("しょ", "sho", 0, 0),

                //T* Monographs
                new HiraganaCharacter("た", "ta", 0, 0),
                new HiraganaCharacter("ち", "chi", 0, 0),
                new HiraganaCharacter("つ", "tsu", 0, 0),
                new HiraganaCharacter("て", "te", 0, 0),
                new HiraganaCharacter("と", "to", 0, 0),

                //Ch* Diagraphs
                new HiraganaCharacter("ちゃ", "cha", 0, 0),
                new HiraganaCharacter("ちゅ", "chu", 0, 0),
                new HiraganaCharacter("ちょ", "cho", 0, 0),

                //N* Monographs
                new HiraganaCharacter("な", "na", 0, 0),
                new HiraganaCharacter("に", "ni", 0, 0),
                new HiraganaCharacter("ぬ", "nu", 0, 0),
                new HiraganaCharacter("ね", "ne", 0, 0),
                new HiraganaCharacter("の", "no", 0, 0),

                //Ny* Diagraphs
                new HiraganaCharacter("にゃ", "nya", 0, 0),
                new HiraganaCharacter("にゅ", "nyu", 0, 0),
                new HiraganaCharacter("にょ", "nyo", 0, 0),

                //H* Monographs
                new HiraganaCharacter("は", "ha", 0, 0),
                new HiraganaCharacter("ひ", "hi", 0, 0),
                new HiraganaCharacter("ふ", "fu", 0, 0),
                new HiraganaCharacter("へ", "he", 0, 0),
                new HiraganaCharacter("ほ", "ho", 0, 0),

                //Hy* Diagraphs
                new HiraganaCharacter("ひゃ", "hya", 0, 0),
                new HiraganaCharacter("ひゅ", "hyu", 0, 0),
                new HiraganaCharacter("ひょ", "hyo", 0, 0),

                //M* Monographs
                new HiraganaCharacter("ま", "ma", 0, 0),
                new HiraganaCharacter("み", "mi", 0, 0),
                new HiraganaCharacter("む", "mu", 0, 0),
                new HiraganaCharacter("め", "me", 0, 0),
                new HiraganaCharacter("も", "mo", 0, 0),

                //My* Diagraphs
                new HiraganaCharacter("みゃ", "mya", 0, 0),
                new HiraganaCharacter("みゅ", "myu", 0, 0),
                new HiraganaCharacter("みょ", "myo", 0, 0),

                //Y* Monographs
                new HiraganaCharacter("や", "ya", 0, 0),
                new HiraganaCharacter("ゆ", "yu", 0, 0),
                new HiraganaCharacter("よ", "yo", 0, 0),

                //R* Monographs
                new HiraganaCharacter("ら", "ra", 0, 0),
                new HiraganaCharacter("り", "ri", 0, 0),
                new HiraganaCharacter("る", "ru", 0, 0),
                new HiraganaCharacter("れ", "re", 0, 0),
                new HiraganaCharacter("ろ", "ro", 0, 0),

                //Ry* Diagraphs
                new HiraganaCharacter("りゃ", "rya", 0, 0),
                new HiraganaCharacter("りゅ", "ryu", 0, 0),
                new HiraganaCharacter("りょ", "ryo", 0, 0),

                //W* Monographs
                new HiraganaCharacter("わ", "wa", 0, 0),
                //new HiraganaCharacter("ゐ", "wi", 0, 0),
                //new HiraganaCharacter("ゑ", "we", 0, 0),
                new HiraganaCharacter("を", "wo", 0, 0),

                //Anomalous N
                new HiraganaCharacter("ん", "n", 0, 0),

                //G* Diacritics
                new HiraganaCharacter("が", "ga", 0, 0),
                new HiraganaCharacter("ぎ", "gi", 0, 0),
                new HiraganaCharacter("ぐ", "gu", 0, 0),
                new HiraganaCharacter("げ", "ge", 0, 0),
                new HiraganaCharacter("ご", "go", 0, 0),

                //Gy* Diagraphs
                new HiraganaCharacter("ぎゃ", "gya", 0, 0),
                new HiraganaCharacter("ぎゅ", "gyu", 0, 0),
                new HiraganaCharacter("ぎょ", "gyo", 0, 0),

                //Z* Diacritics
                new HiraganaCharacter("ざ", "za", 0, 0),
                new HiraganaCharacter("じ", "ji", 0, 0),
                new HiraganaCharacter("ず", "zu", 0, 0),
                new HiraganaCharacter("ぜ", "ze", 0, 0),
                new HiraganaCharacter("ぞ", "zo", 0, 0),

                //J* Diagraphs
                new HiraganaCharacter("じゃ", "ja", 0, 0),
                new HiraganaCharacter("じゅ", "ju", 0, 0),
                new HiraganaCharacter("じょ", "jo", 0, 0),

                //D* Diacritics
                new HiraganaCharacter("だ", "da", 0, 0),
                //new HiraganaCharacter("ぢ", "ji", 0, 0),
                //new HiraganaCharacter("づ", "zu", 0, 0),
                new HiraganaCharacter("で", "de", 0, 0),
                new HiraganaCharacter("ど", "do", 0, 0),

                //J* Diagraphs
                //new HiraganaCharacter("ぢゃ", "ja", 0, 0),
                //new HiraganaCharacter("ぢゅ", "ju", 0, 0),
                //new HiraganaCharacter("ぢょ", "jo", 0, 0),

                //B* Diacritics
                new HiraganaCharacter("ば", "ba", 0, 0),
                new HiraganaCharacter("び", "bi", 0, 0),
                new HiraganaCharacter("ぶ", "bu", 0, 0),
                new HiraganaCharacter("べ", "be", 0, 0),
                new HiraganaCharacter("ぼ", "bo", 0, 0),

                //By* Diagraphs
                new HiraganaCharacter("びゃ", "bya", 0, 0),
                new HiraganaCharacter("びゅ", "byu", 0, 0),
                new HiraganaCharacter("びょ", "byo", 0, 0),

                //P* Diacritics
                new HiraganaCharacter("ぱ", "pa", 0, 0),
                new HiraganaCharacter("ぴ", "pi", 0, 0),
                new HiraganaCharacter("ぷ", "pu", 0, 0),
                new HiraganaCharacter("ぺ", "pe", 0, 0),
                new HiraganaCharacter("ぽ", "po", 0, 0),

                //Py* Diagraphs
                new HiraganaCharacter("ぴゃ", "pya", 0, 0),
                new HiraganaCharacter("ぴゅ", "pyu", 0, 0),
                new HiraganaCharacter("ぴょ", "pyo", 0, 0)
            };
        }

        /// <summary>
        /// Populate the StatsView ListView control with blank entries.
        /// </summary>
        private void PopulateStats()
        {
            foreach (HiraganaCharacter c in HiraganaCharacters)
            {
                StatsView.Items.Add(new ListViewItem(new string[] {c.Hiragana, c.Romaji, "", ""}));
            }
        }

        #endregion

        #region Trainer Tab

        /// <summary>
        /// Selects a new character when the 'Next' button is clicked.
        /// </summary>
        private void TrainerButtonNext_Click(object sender, EventArgs e)
        {
            CurrentChar = Rand.Next(0, HiraganaCharacters.Length);

            TrainerHiraganaDisplay.Text = HiraganaCharacters[CurrentChar].Hiragana;
            TrainerRomajiInput.BackColor = Color.White;
            TrainerRomajiInput.Text = "";

            TrainerButtonCheck.Enabled = true;
            TrainerButtonDontKnow.Enabled = true;
            TrainerRomajiInput.Enabled = true;
            TrainerButtonNext.Enabled = false;
        }

        /// <summary>
        /// Checks the current answer when the 'Check' button is clicked.
        /// </summary>
        private void TrainerButtonCheck_Click(object sender, EventArgs e)
        {
            if (TrainerRomajiInput.Text.ToLower().Equals(HiraganaCharacters[CurrentChar].Romaji))
            {
                HiraganaCharacters[CurrentChar].RegisterAttempt(1);
                TrainerRomajiInput.BackColor = Color.DarkGreen;

            }
            else
            {
                TrainerRomajiInput.Text = HiraganaCharacters[CurrentChar].Romaji;
                HiraganaCharacters[CurrentChar].RegisterAttempt(0);
                TrainerRomajiInput.BackColor = Color.Maroon;
            }

            TrainerButtonCheck.Enabled = false;
            TrainerButtonDontKnow.Enabled = false;
            TrainerRomajiInput.Enabled = false;
            TrainerButtonNext.Enabled = true;
        }

        /// <summary>
        /// Replaces the current answer with the correct one when the 'Don't Know' button is clicked.
        /// </summary>
        private void TrainerButtonDontKnow_Click(object sender, EventArgs e)
        {
            TrainerRomajiInput.Text = HiraganaCharacters[CurrentChar].Romaji;
            HiraganaCharacters[CurrentChar].RegisterAttempt(0.5f);
            TrainerRomajiInput.BackColor = Color.Maroon;

            TrainerButtonCheck.Enabled = false;
            TrainerButtonDontKnow.Enabled = false;
            TrainerRomajiInput.Enabled = false;
            TrainerButtonNext.Enabled = true;
        }

        #endregion

        #region Stats Tab

        /// <summary>
        /// Loads the previous stats data from the Stats.dat file
        /// </summary>
        private void LoadStats()
        {
            if (File.Exists(StatsFile))
            {
                try
                {
                    //Stored using BinaryReader/BinaryWriter to save a little bit of storage space.
                    BinaryReader reader = new BinaryReader(new FileStream(StatsFile, FileMode.Open));

                    for (int i = 0; i < HiraganaCharacters.Length; i++)
                    {
                        int totalAttempts = reader.ReadInt32();
                        float curSuccessLevel = reader.ReadSingle();
                        HiraganaCharacters[i].LoadData(totalAttempts, curSuccessLevel);
                    }

                    reader.Close();
                }
                catch (Exception e)
                {
                    MessageBox.Show("Could not load stats file.", "Error Loading Stats", MessageBoxButtons.OK);
                    Close();
                }
            }
            else
            {
                File.Create(StatsFile).Close();
                SaveStats();
            }
        }

        /// <summary>
        /// Saves the current stats to the Stats.dat file
        /// </summary>
        private void SaveStats()
        {
            if (File.Exists(StatsFile))
            {
                try
                {
                    BinaryWriter writer = new BinaryWriter(new FileStream(StatsFile, FileMode.Create));

                    for (int i = 0; i < HiraganaCharacters.Length; i++)
                    {
                        writer.Write(HiraganaCharacters[i].TotalAttempts);
                        writer.Write(HiraganaCharacters[i].curSuccessLevel);
                    }

                    writer.Flush();
                    writer.Close();
                }
                catch (Exception e)
                {
                    MessageBox.Show("Could not save stats file.", "Error Saving Stats", MessageBoxButtons.OK);
                }
            }
            else
            {
                File.Create(StatsFile).Close();
                SaveStats();
            }
        }


        /// <summary>
        /// Updates the StatsView entries with the latest data. Called when the tab changes.
        /// </summary>
        private void UpdateStats(object sender, EventArgs e)
        {
            if (MainWindowTabs.SelectedIndex == 1)
            {
                for (int i = 0; i < HiraganaCharacters.Length; i++)
                {
                    StatsView.Items[i].SubItems[0].Text = HiraganaCharacters[i].Hiragana;
                    StatsView.Items[i].SubItems[1].Text = HiraganaCharacters[i].Romaji;

                    int totalAttempts = HiraganaCharacters[i].TotalAttempts;
                    StatsView.Items[i].SubItems[2].Text = totalAttempts.ToString();

                    if (totalAttempts > 0)
                    {
                        float successLevel = HiraganaCharacters[i].SuccessLevel;
                        if (successLevel >= 0.8f)
                        {
                            StatsView.Items[i].BackColor = Color.FromArgb(127, 255, 127);
                        }
                        else if (successLevel <= 0.3f)
                        {
                            StatsView.Items[i].BackColor = Color.FromArgb(255, 127, 127);
                        }
                        else
                        {
                            StatsView.Items[i].BackColor = Color.FromArgb(255, 255, 127);
                        }

                        int rating = (int)(successLevel * 5f);
                        if (rating > 4) rating = 4;
                        StatsView.Items[i].SubItems[3].Text = Ratings[rating];
                    }
                    else
                    {
                        StatsView.Items[i].BackColor = Color.LightGray;
                        StatsView.Items[i].SubItems[3].Text = "?";
                    }
                }
            }
        }

        #endregion
    }
}
